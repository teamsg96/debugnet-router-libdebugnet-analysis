/*
 * factual.cpp
 *
 *  Created on: 11 Mar 2019
 *      Author: sgasper
 */

#include "factual.h"

/*
 * Generates a message explaining what the current internet speed is
 */
FactualQueryResult queryGetCurrentInternetSpeed(){
	double avgBwbps;		// Bits per second
	double avgLoadbps;		// Bits per second
	double capacityUsed;	// Percentage
	FactualQueryResult qr;
	Logger logger;
	string logHandle = "currentSpeed";

	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	logger.log(logHandle, "Querying \'What is the current internet speed?\'");

	// Get the speed for each direction
	for (DATA_DIRECTION dir : {DATA_UP, DATA_DOWN}){
		// Add title for the current direction
		string title = DATA_DIR_TO_STRING.find(dir)->second + " Speed: ";
		// Capitalise first letter of direction
		title[0] = toupper(title[0]);
		title = makeBold(title);
		qr.factualStr += title;

		if (getIspBandwidthAvgStats(dir, avgBwbps, avgLoadbps, capacityUsed,
				CURRENT_SPEED_SECONDS_PERIOD) == 0){
			// Record speed in Mbps
			qr.factualStr += makeItalic(to_string(
					static_cast<int>(bitsToMegaBits(avgBwbps))) + " Mbps");
		}
		else{
			// It was not possible to get values. This is likely because no data
			// was captured in given time frame.
			qr.factualStr += "unable to measure";
		}

		qr.factualStr += NL;
	}

	logger.log(logHandle, "response: " + qr.factualStr);

	qr.code = QUERY_SUCCESS;

	return qr;
}

/*
 * Generates a message discussing ISP performance
 */
FactualQueryResult queryGetIspPerformance(double expUpMbps, double expDownMbps){
	double avgBwbps;		// Bits per second
	double avgBwMbps;		// Mega bits per second
	double expBwMbps;		// Mega bits per second
	double avgLoadbps;		// Bits per second
	double capacityUsed;	// Percentage
	FactualQueryResult qr;
	Logger logger;
	string logHandle = "ispDelivery";

	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	logger.log(logHandle, "Querying \'Internet service provider performance report\'");
	logger.log(logHandle, "expected uploadSpeed=" + to_string(expUpMbps));
	logger.log(logHandle, "expected uploadDown=" + to_string(expDownMbps));

	// Get Service information for each direction of communication
	for (DATA_DIRECTION dir : {DATA_UP, DATA_DOWN}){
		stringstream capStr;
		stringstream avgBwMbpsStr;
		stringstream dirInfo;
		string dirString = DATA_DIR_TO_STRING.find(dir)->second;

		// Add title for the current direction
		string sectionTitle = dirString + " Speed Performance";
		// Capitalise first letter of direction
		sectionTitle[0] = toupper(sectionTitle[0]);
		sectionTitle = makeBold(sectionTitle);
		qr.factualStr += sectionTitle + ":" + NL;

		// Determine the expected bandwidth value
		if (dir == DATA_UP){
			expBwMbps = expUpMbps;
		}
		else{
			expBwMbps = expDownMbps;
		}

		// Get bandwidth stat's for a given time period
		if (getIspBandwidthAvgStats(dir, avgBwbps, avgLoadbps, capacityUsed,
				PERIOD_TO_CHECK_SERVICE_DELIVER) == 0){
			avgBwMbps = bitsToMegaBits(avgBwbps);

			// produce string forms of some of the stat's
			avgBwMbpsStr << fixed << setprecision(2) << avgBwMbps << "Mbps";
			capStr << fixed << setprecision(2) << capacityUsed << "%";
		}
		else{
			qr.factualStr += "Information not available at this time." + NL + NL;
			continue;
		}

		// Derive a string that describes the difference between the expected
		// speed and the actual speed
		string speedDiffStrDescription;
		if (expBwMbps > avgBwMbps){
			// Actual bandwidth measurement is less than what was expected
			double speedDiffPercent = 100.0 - ((avgBwMbps / expBwMbps) * 100.0);

			if (speedDiffPercent <= SIMILAR_MARGIN_PERCENT){
				speedDiffStrDescription = "similar";
			}
			else{
				speedDiffStrDescription = "much worse";
			}
		}
		else{
			// Actual bandwidth measurement is more than what was expected
			double speedDiffPercent = 100.0 - ((expBwMbps / avgBwMbps) * 100.0);

			if (speedDiffPercent <= SIMILAR_MARGIN_PERCENT){
				speedDiffStrDescription = "similar";
			}
			else{
				speedDiffStrDescription = "much better";
			}
		}

		// Every time the direction is used it should be in bold and italic
		dirString = makeItalic(makeBold(dirString));

		dirInfo << "On average your network is receiving " <<
				makeItalic(makeBold(avgBwMbpsStr.str())) << " " << dirString <<
				" speed from your internet service provider and you where " <<
				"promised an " << dirString << " speed of " <<
				makeItalic(makeBold(to_string(static_cast<int>(expBwMbps)) + "Mbps")) <<
				". You are getting " << makeItalic(makeBold(speedDiffStrDescription)) <<
				" speeds to what you where promised." << NL <<
				"On average your network is using " <<
				makeItalic(makeBold(capStr.str())) << " of the real " <<
				dirString << " speed detected by DebugNet. This means ";

		// Inform on how close the network usage is to the bandwidth limit
		if (capacityUsed < CLOSE_TO_CAPACITY_MARK){
			dirInfo << "your network isn't running close to " << dirString <<
					" speed capacity. So you should not experience any " <<
					dirString << " speed related issues.";
		}
		else{
			if (capacityUsed < 100.0){
				dirInfo << "your network is running very close to the ";
			}
			else{
				dirInfo << "your network is running at the ";
			}

			dirInfo << dirString << " speed limit set by your internet "
					"service provider. This will likely cause performance "
					"issues with your internet. Consider contacting your "
					"service provider to raise the " << dirString << " limit.";
		}

		dirInfo << NL + NL;

		qr.factualStr += dirInfo.str();
	}

	logger.log(logHandle, "response: " + qr.factualStr);

	qr.code = QUERY_SUCCESS;

	return qr;
}

/*
 * Generates a message explaining what the best and worst time of day is for the
 * internet
 */
FactualQueryResult queryGetTimeOfDayInternetFastestSlowest(){
	FactualQueryResult qr;
	Logger logger;
	string logHandle = "timeOfDayInternet";

	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	logger.log(logHandle, "Querying \'What time of day is best and worst for "
			"internet speed?\'");

	// Repeat for upload and download speed
	for (DATA_DIRECTION dir : {DATA_DOWN, DATA_UP}){
		// Add title for the current direction
		string sectionTitle = DATA_DIR_TO_STRING.find(dir)->second + " Speed:";
		// Capitalise first letter of direction
		sectionTitle[0] = toupper(sectionTitle[0]);
		sectionTitle = makeBold(sectionTitle);
		qr.factualStr += sectionTitle + NL;

		// Get average BW rating for each period of time in a day
		vector<BWTimeRange> speedRanges = getAvgIspBandwidthForTimesOfDay(
				dir, DAYS_TO_CHECK_ISP_BW, TIME_FRAME_SIZE);

		// If no speed data was found for the direction then inform that
		// not enough data has been collected for the speed direction.
		if (speedRanges.size() <= 0){
			qr.factualStr += ("The router has not collected enough data " \
									"yet to determine this." + NL + NL);
			continue;
		}

		// Include in the output the best and worst times of the day
		for (string qualityStr : vector<string> {"best", "worst"}){
			qr.factualStr += TAB + "The " + makeBold(qualityStr) + " times of the day: ";

			if (qualityStr == "best"){
				sort(speedRanges.begin(), speedRanges.end(), compareSpeedTimeRangeStructForBest);
			}
			else{
				sort(speedRanges.begin(), speedRanges.end(), compareSpeedTimeRangeStructForWorst);
			}

			// Output each top time frame with an average BW rating.
			vector<string> formattedTimeStrs;
			for (int i = 0;
					i<NUM_OF_TIME_FRAMES_TO_SHOW && i<speedRanges.size();
					i++){
				formattedTimeStrs.push_back(
						convert24HrTo12Hr(speedRanges[i].frmHour) + " to " +
						convert24HrTo12Hr(speedRanges[i].toHour) +
						" (average: " + makeItalic(to_string(static_cast<int>(
								bitsToMegaBits(speedRanges[i].avgSpeed))) +
								"Mbps") + ")");
			}

			qr.factualStr += generateOrderedString(formattedTimeStrs, 2,
					true, false, false, false, false) + NL;
		}

		qr.factualStr +=  NL;
	}

	logger.log(logHandle, "response: " + qr.factualStr);

	qr.code = QUERY_SUCCESS;

	return qr;
}
