/*
 * TroubleshootReport.cpp
 *
 *  Created on: 8 Mar 2019
 *      Author: sgasper
 */

#include "TroubleshootReport.h"

TroubleshootReport::TroubleshootReport(){

}

TroubleshootReport::TroubleshootReport(string queryName, Logger logger) {
	this->queryName = queryName;
	this->logger = logger;

	logger.log("Initialising report for troubleshoot query: " + queryName);
}

/*
 * Add background information to the troubleshoot request and response
 */
void TroubleshootReport::addContext(string contextName, bool contextValue){
	string boolStr;

	if (contextValue == true){
		boolStr = "true";
	}
	else{
		boolStr = "false";
	}

	addContext(contextName, boolStr);
}

/*
 * Add background information to the troubleshoot request and response
 */
void TroubleshootReport::addContext(string contextName, int contextValue){
	addContext(contextName, to_string(contextValue));
}

/*
 * Add background information to the troubleshoot request and response
 */
void TroubleshootReport::addContext(string contextName, string contextValue){
	logger.log(CONTEXT_LOG_HANDLE, contextName + "=" + contextValue);
	context[contextName] = contextValue;
}

/*
 * Convert report into a JSON string
 */
string TroubleshootReport::getAsJsonStr() {
	Document reportJson;
	reportJson.SetObject();
	Document::AllocatorType& allocator = reportJson.GetAllocator();

	// Add the name of the query
	reportJson.AddMember(
			"query_name",
			Value(queryName.c_str(), allocator),
			allocator);

	// Add context of the troubleshoot query
	Value contextValues(kObjectType);
	for (auto const& c : context){
		contextValues.AddMember(
				Value(c.first.c_str(), allocator),
				Value(c.second.c_str(), allocator),
				allocator);

	}
	reportJson.AddMember("context", contextValues, allocator);

	// Add information about each problem detected
	Value problemValues(kObjectType);
	Value severeProblemValues(kArrayType);
	Value moderateProblemValues(kArrayType);
	for (DetectedProblem p : problems){
		// Format all details for problem into a JSON object
		Value problemDetails(kObjectType);
		problemDetails.AddMember(
				"problem",
				Value(p.problem.c_str(), allocator),
				allocator);
		problemDetails.AddMember(
				"advice",
				Value(p.advice.c_str(), allocator),
				allocator);

		// Push the problem details onto the array for the level of severity
		if (p.severity == SEVERE_PROBLEM){
			severeProblemValues.PushBack(problemDetails, allocator);
		}
		else{
			moderateProblemValues.PushBack(problemDetails, allocator);
		}
	}

	// Attach the arrays to the main Json report object
	problemValues.AddMember("severe", severeProblemValues, allocator);
	problemValues.AddMember("moderate", moderateProblemValues, allocator);
	reportJson.AddMember("problems", problemValues, allocator);

	// Convert the Json object into a string
	StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    reportJson.Accept(writer);

    return string(buffer.GetString());
}


/*
 * Report a problem and provide details.
 */
void TroubleshootReport::reportProblem(string problem,
		PROBLEM_SEVERITY severity, string advice) {
	// Log the problem found
	string logMsg = "";
	logMsg += "problem found...";
	logMsg += "\n\tproblem:" + problem;
	logMsg += "\n\tseverity: " + SEVERITY_TO_STRING.find(severity)->second;
	logMsg += "\n\tadvice: " + advice;
	logger.log(logMsg);

	// Store problem details
	DetectedProblem problemDetails;
	problemDetails.problem = problem;
	problemDetails.severity = severity;
	problemDetails.advice = advice;
	problems.push_back(problemDetails);
}

