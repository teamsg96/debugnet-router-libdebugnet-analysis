/*
 * problems.h
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */

#ifndef PROBLEMS_H_
#define PROBLEMS_H_

#include <iostream>
using namespace std;

#include "debugnet.h"
#include "analysis.h"
#include "factual.h"

// For measuring how severe a problem detected is
enum PROBLEM_SEVERITY{
	NO_PROBLEM,
	MODERATE_PROBLEM,
	SEVERE_PROBLEM
};

const map<PROBLEM_SEVERITY, string> SEVERITY_TO_STRING = {
		{NO_PROBLEM, "no problem"},
		{MODERATE_PROBLEM, "moderate"},
		{SEVERE_PROBLEM, "severe"}
};

const double BANDWIDTH_MODERATE_CAPACITY = 90.0;			// Percent
const double BANDWIDTH_LOW_CAPACITY = 95.0;					// Percent
const int LATENCY_BAD = 100;								// ms
const double NET_LATENCY_PROPORTION_MODERATE = 50;			// Percent
const double NET_LATENCY_PROPORTION_BAD = 60.0;				// Percent
const double NET_WIRELESS_SIGNAL_PROPORTION_MODERATE = 50;	// Percent
const double NET_WIRELESS_SIGNAL_PROPORTION_BAD = 60;		// Percent
const int WIRELESS_SIGNAL_STRENGTH_MODERATE = 70;			// dBm
const int WIRELESS_SIGNAL_STRENGTH_BAD = 80;				// dBm

PROBLEM_SEVERITY checkForBandwidthCapacityProblems(int secondsPeriod,
		vector<DATA_DIRECTION>& dirsAtCap, Logger logger);
PROBLEM_SEVERITY checkForDeviceLatencyProblems(Device device, int secondsPeriod,
		Logger logger);
PROBLEM_SEVERITY checkForDeviceWifiSignalProblems(Device device, int secondsPeriod,
		Logger logger);
PROBLEM_SEVERITY checkForISPLatencyProblems(int secondsPeriod,
		Logger logger);
PROBLEM_SEVERITY checkForNetworkLatencyProblems(vector<Device> devices,
		int secondsPeriod, vector<Device>& devicesWithLatProblems, Logger logger);
PROBLEM_SEVERITY checkForNetworkWifiSignalProblems(vector<Device> devices,
		int secondsPeriod, vector<Device>& devicesWithSignalProblems,
		Logger logger);
#endif /* PROBLEMS_H_ */
