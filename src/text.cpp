/*
 * text.cpp
 *
 *  Created on: 27 Mar 2019
 *      Author: sgasper
 */

#include "text.h"

/*
 * Convert a 24 hour value to a 12 hour value string
 */
string convert24HrTo12Hr(int hour){
	if (hour == 0){
		return "12am";
	}
	else if (hour == 12){
		return "12pm";
	}
	else if (hour < 12){
		return to_string(hour) + "am";
	}
	else{
		return to_string(hour - 12) + "pm";
	}

}

/*
 * Creates a string list from a vector list.
 */
string generateOrderedString(vector<string> list, int numOfTabs, bool numbered,
		bool boldNumbers, bool boldContent, bool italicContent, bool lineGap){
	string listStr = "";

	// Add each item to the string
	for (unsigned int i = 0; i < list.size(); i++){
		// Start on a new line
		listStr += NL;

		// Create tab(s) before writing any content (if set)
		for (int t = 0; t < numOfTabs; t++){
			listStr += TAB;
		}

		// Create a numbered entry or just a listed entry.
		if (numbered){
			// Make the numbers bold if enabled.
			if (boldNumbers){
				listStr += makeBold(to_string(i+1) + ") ");
			}
			else{
				listStr += to_string(i+1) + ") ";
			}
		}
		else{
			listStr += "- ";
		}

		string content = list[i];

		if (boldContent){
			content = makeBold(content);
		}

		if (italicContent){
			content = makeItalic(content);
		}

		listStr += content;

		// Create an extra gap between each item if enabled.
		if (lineGap){
			listStr += NL;
		}

	}

	return listStr;
}

/*
 * Use HTML syntax to make text bold
 */
string makeBold(string txt){
	return "<b>" + txt + "</b>";
}

/*
 * Use HTML syntax to make text italic
 */
string makeItalic(string txt){
	return "<i>" + txt + "</i>";
}
