/*
 * problems.cpp
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */

#include "problems.h"

/*
 * Check if the bandwidth capacity is a problem. If their is to much traffic
 * for the ISP bandwidth to handle then this is a problem.
 */
PROBLEM_SEVERITY checkForBandwidthCapacityProblems(int secondsPeriod,
		vector<DATA_DIRECTION>& dirsAtCap, Logger logger){
	double avgBwbps;		// Bits per second
	double avgLoadbps;		// Bits per second
	double capacityUsed;	// Percentage
	PROBLEM_SEVERITY severity;
	string logHandle = "bwCapacityCheck";

	severity = NO_PROBLEM;

	vector<DATA_DIRECTION> dataDirs = {DATA_UP, DATA_DOWN};

	for (DATA_DIRECTION dir : dataDirs){
		string dirLogHandle;

		if (dir == DATA_UP){
			dirLogHandle = logHandle + ": up";
		}
		else{
			dirLogHandle = logHandle + ": down";
		}

		if (getIspBandwidthAvgStats(dir, avgBwbps, avgLoadbps, capacityUsed,
				secondsPeriod) != 0){
			// It was not possible to get values. This is likely because no data
			// was captured in given time frame. So we can not assume a problem is
			// present with no data.
			logger.log(dirLogHandle, "no bw/load readings for time frame");
			continue;
		}

		logger.log(dirLogHandle,
				"capacityBw=" + to_string(bitsToMegaBits(avgBwbps)) + "Mbps, " +
				"load=" + to_string(bitsToMegaBits(avgLoadbps)) + "Mbps, " +
				"capacityUsed=" + to_string(capacityUsed) + "%");

		// Check if the remaining capacity is to low and is thus a problem.
		if (capacityUsed >= BANDWIDTH_MODERATE_CAPACITY &&
				capacityUsed < BANDWIDTH_LOW_CAPACITY){
			// The highest problem level is recorded. If another problem has
			// been found at a lower severity then its ignored.
			if (severity != SEVERE_PROBLEM){
				severity = MODERATE_PROBLEM;
			}

			dirsAtCap.push_back(dir);
		}
		else if (capacityUsed >= BANDWIDTH_LOW_CAPACITY){
			// Critical problem found so their is no need to check anything else
			severity = SEVERE_PROBLEM;
			dirsAtCap.push_back(dir);
		}

	}
	return severity;
}

/*
 * Check for problems with device latency
 */
PROBLEM_SEVERITY checkForDeviceLatencyProblems(Device device, int secondsPeriod,
		Logger logger){
	double avgLatency;
	PROBLEM_SEVERITY severity;
	string logHandle = "deviceLatencyCheck: " + device.getMacAddress();

	// Add a filter so that the average is only computed for the device in question
	vector<string> filters = {"device=\'"+device.getMacAddress()+"\'"};

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Get the average latency for the device over the period of time. If no
	// average could be calculated then it can't be determined if this is a
	// problem.
	if(getAvgMetricFieldValue(LATENCY_DEVICE_TABLE, "avg_latency",
			timeFrom, timeNow, avgLatency, filters) == 1){
		logger.log(logHandle, "no avg latency for time period");
		return NO_PROBLEM;
	}

	logger.log(logHandle, "avg latency=" + to_string(avgLatency) + "ms");

	// Determine the severity of the latency
	if (avgLatency < LATENCY_BAD){
		severity = NO_PROBLEM;
	}
	else {
		severity = SEVERE_PROBLEM;
	}

	return severity;
}

/*
 * Check for problems with ISP latency. This is measured from the router to a
 * test server near the ISP.
 */
PROBLEM_SEVERITY checkForISPLatencyProblems(int secondsPeriod, Logger logger){
	double avgLatency;
	PROBLEM_SEVERITY severity;
	string logHandle = "ispLatencyCheck";

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Get the average latency for the ISP over the period of time. If no
	// average could be calculated then it can't be determined if this is a
	// problem.
	if(getAvgMetricFieldValue(LATENCY_ISP_TABLE, "latency",
			timeFrom, timeNow, avgLatency) == 1){
		logger.log(logHandle, "no avg latency for time period");
		return NO_PROBLEM;
	}

	logger.log(logHandle, "avg latency=" + to_string(avgLatency) + "ms");

	// Determine the severity of the latency
	if (avgLatency < LATENCY_BAD){
		severity = NO_PROBLEM;
	}
	else {
		severity = SEVERE_PROBLEM;
	}

	return severity;
}

/*
 * Check for problems with the signal strength of a device
 */
PROBLEM_SEVERITY checkForDeviceWifiSignalProblems(Device device, int secondsPeriod,
		Logger logger){
	double avgSignal;
	PROBLEM_SEVERITY severity;
	string logHandle = "deviceSignalStrengthCheck" + device.getMacAddress();

	// Add a filter so that the average is only computed for the device in question
	vector<string> filters = {"device=\'"+device.getMacAddress()+"\'"};

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Get the average signal strength for the device over the period of time.
	// If no average could be calculated then it can't be determined if this is a
	// problem.
	if (getAvgMetricFieldValue(WIRELESS_SIGNAL_TABLE, "signal",
			timeFrom, timeNow, avgSignal, filters) == 1){
		logger.log(logHandle, "no avg signal strength for time period");
		return NO_PROBLEM;
	}

	logger.log(logHandle, "avg signal strength: -" + to_string(avgSignal) + "dBm");

	// Determine the severity of the wireless signal
	if (avgSignal < WIRELESS_SIGNAL_STRENGTH_MODERATE){
		severity = NO_PROBLEM;
	}
	else if (avgSignal >= WIRELESS_SIGNAL_STRENGTH_MODERATE &&
				avgSignal < WIRELESS_SIGNAL_STRENGTH_BAD){
		severity = MODERATE_PROBLEM;
	}
	else{
		severity = SEVERE_PROBLEM;

	}

	return severity;
}

/*
 * Check if problems exist with latency on a global scale across the network
 */
PROBLEM_SEVERITY checkForNetworkLatencyProblems(vector<Device> devices,
		int secondsPeriod, vector<Device>& devicesWithLatProblems, Logger logger){
	PROBLEM_SEVERITY severity;
	string logHandle = "networkLatencyCheck";

	// If their are no devices then their are no problems
	if (devices.empty() == true){
		return NO_PROBLEM;
	}

	// Check how many devices have latency issues.
	for (Device device : devices){
		if (checkForDeviceLatencyProblems(device, secondsPeriod, logger) != NO_PROBLEM){
			devicesWithLatProblems.push_back(device);
		}
	}

	logger.log(logHandle, "number of devices found: " + to_string(devices.size()));
	logger.log(logHandle, "number of devices with latency problems: " +
			to_string(devicesWithLatProblems.size()));

	// Calculate the proportion of problems across all devices
	double proportion = ((double)devicesWithLatProblems.size() / (double)devices.size()) * 100.0;

	logger.log(logHandle, "proportion of devices with latency problems: " +
			to_string(proportion));

	// Determine if the number of devices experiencing latency issues is a concern
	if (proportion < NET_LATENCY_PROPORTION_MODERATE){
		severity = NO_PROBLEM;
	}
	else if (proportion >= NET_LATENCY_PROPORTION_MODERATE &&
			proportion < NET_LATENCY_PROPORTION_BAD){
		severity = MODERATE_PROBLEM;
	}
	else{
		severity = SEVERE_PROBLEM;
	}

	return severity;
}

/*
 * Check if problems exist with wireless signal on a global scale of the network
 */
PROBLEM_SEVERITY checkForNetworkWifiSignalProblems(vector<Device> devices,
		int secondsPeriod, vector<Device>& devicesWithSignalProblems, Logger logger){
	int numberOfWirelessDevices = 0;
	PROBLEM_SEVERITY severity;
	string logHandle = "networkSignalStrengthCheck";

	// If their are no devices then their are no problems
	if (devices.empty() == true){
		return NO_PROBLEM;
	}

	// Check how many devices are wireless and how many of them have signal
	// issues.
	for (Device device : devices){
		if (device.isWireless() == false){
			continue;
		}

		numberOfWirelessDevices++;

		if (checkForDeviceWifiSignalProblems(device, secondsPeriod, logger) != NO_PROBLEM){
			devicesWithSignalProblems.push_back(device);
		}
	}

	logger.log(logHandle, "number of wireless devices found: " +
			to_string(numberOfWirelessDevices));
	logger.log(logHandle, "number of wireless devices with signal problems: " +
			to_string(devicesWithSignalProblems.size()));

	// Calculate the proportion of signal problems across all wireless devices
	double proportion = ((double)devicesWithSignalProblems.size() / (double)numberOfWirelessDevices) * 100.0;

	logger.log(logHandle, "proportion of wireless devices with signal problems: " +
			to_string(proportion));

	// Determine if the number of devices experiencing signal issues is a concern
	if (proportion < NET_WIRELESS_SIGNAL_PROPORTION_MODERATE){
		severity = NO_PROBLEM;
	}
	else if (proportion >= NET_WIRELESS_SIGNAL_PROPORTION_MODERATE &&
			proportion < NET_WIRELESS_SIGNAL_PROPORTION_BAD){
		severity = MODERATE_PROBLEM;
	}
	else{
		severity = SEVERE_PROBLEM;
	}

	return severity;
}

