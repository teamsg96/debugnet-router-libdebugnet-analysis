/*
 * troubleshoot.h
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */

#ifndef TROUBLESHOOT_H_
#define TROUBLESHOOT_H_

#include "analysis.h"
#include "problems.h"
#include "results.h"
#include "troubleshoot-responses.h"
#include "TroubleshootReport.h"

// Number of seconds to add to period of time provided by the user
const int ADDITIONAL_PERIOD = 10;				// Seconds


// Results of troubleshoot
struct TSQueryResult{
	QUERY_RESULT_CODE code;
	string err;
	TroubleshootReport report;
};

TSQueryResult queryWhyDoesConnectionDrop(Device device, int secondsOfinstability);
TSQueryResult queryWhyIsDeviceSlow(Device device, int secondsOfSlowness);
TSQueryResult queryWhyIsEntireNetworkSlow(int secondsOfSlowness);


#endif /* TROUBLESHOOT_H_ */
