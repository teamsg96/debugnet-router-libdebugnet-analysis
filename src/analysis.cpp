/*
 * analysis.cpp
 *
 *  Created on: 13 Mar 2019
 *      Author: sgasper
 */

#include "analysis.h"

/*
 * Compares two device tuples to see if one has a greater metric value than
 * the other.
 */
bool compareDeviceMetricTuples(const tuple<int, Device>& left,
		const tuple<int, Device>& right){
	return get<0>(left) > get<0>(right);
}

/*
 * Compares two speed time range structs to see if one has a greater speed
 * value than the other.
 */
bool compareSpeedTimeRangeStructForBest(const BWTimeRange& left,
		const BWTimeRange& right){
	return left.avgSpeed > right.avgSpeed;
}

/*
 * Compares two speed time range structs to see if one has a lesser speed
 * value than the other.
 */
bool compareSpeedTimeRangeStructForWorst(const BWTimeRange& left,
		const BWTimeRange& right){
	return left.avgSpeed < right.avgSpeed;
}

/*
 * Collects averaged stat's about the ISP bandwidth for a given direction of
 * communication for a given period of time. Stat's include the capacity
 * bandwidth of direction, how much load is using it and what proportion
 * of the capacity is being used.
 */
int getIspBandwidthAvgStats(DATA_DIRECTION dir, double& bwCap, double& load,
		double& capacityUsed, int secondsPeriod){
	double avgLoadBps;	// Bytes per second
	string bwField;
	string loadField;

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	if (dir == DATA_UP){
		bwField = "up";
		loadField = "bps_out";
	}
	else{
		bwField = "down";
		loadField = "bps_in";
	}

	// Get the average ISP bandwidth and average traffic load of directional
	// traffic for the period of time.
	if (getAvgMetricFieldValue(BANDWIDTH_ISP_TABLE, bwField,
				timeFrom, timeNow, bwCap) == 1 ||
			getAvgMetricFieldValue(TRAFFIC_LOAD_ENT_NET_TABLE, loadField,
				timeFrom, timeNow, avgLoadBps) == 1){
		// It was not possible to get an average for either value, so the
		// capacity cannot be checked for the current direction, This is
		// likely because no data was captured in given time frame.
		return 1;
	}

	load = bytesToBits(avgLoadBps);

	// The bandwidth measurement is actually the available bandwidth of the
	// ISP, opposed to the bandwidth capacity. Available bandwidth is the
	// amount of bandwidth that is available at any one time whilst traffic
	// is passing through and the capacity bandwidth is the maximum
	// achievable bandwidth when no traffic is running through the network.
	// To know how much of a proportion the traffic load is taking up
	// compared to the capacity of bandwidth offered by the ISP, bandwidth
	// capacity needs to be known. The average traffic load from time is
	// added to the available bandwidth to get bandwidth capacity.
	bwCap += load;

	// Calculate how much of the capacity was used
	capacityUsed = (load / bwCap) * 100;

	return 0;
}

/*
 * Get the average bandwidth for each time period in a day over a given number
 * of days.
 */
vector<BWTimeRange> getAvgIspBandwidthForTimesOfDay(DATA_DIRECTION dir,
		int daysToCheck, int hoursInRange){
	string tableField;
	vector<BWTimeRange> allSpeedRanges;
	vector<BWTimeRange> validSpeedRanges;

	time_t now = time(0);

	tm *theTime = gmtime(&now);
	// Change time so that its at the very start of today
	theTime->tm_sec=0;
	theTime->tm_min=0;
	theTime->tm_hour=0;

	// Go back the set number of days
	theTime->tm_hour -= (daysToCheck * 24);

	// Calculate the Unix time stamps for every time period for the last given
	// number of days.
	for (int d=0; d < daysToCheck; d++){
		for (int h=0; h<24; h+= hoursInRange){
			// Create BW range struct for the current hour range if its the first
			// cycle in the parent loop.
			if (d==0){
				BWTimeRange newBtr = {};
				newBtr.frmHour = h;
				newBtr.toHour = h + hoursInRange;
				allSpeedRanges.push_back(newBtr);
			}

			// Get the Unix time stamp for the hour range in day
			time_t frmTime = mktime(theTime);
			theTime->tm_hour += hoursInRange;
			time_t toTime = mktime(theTime);

			// Calculate the expected index for the struct in the list
			int btrIndx = h / hoursInRange;

			// Add the time range stamp for day to the relevant struct.
			allSpeedRanges[btrIndx].timeStamps.push_back(make_tuple(
					static_cast<int>(frmTime), static_cast<int>(toTime)));
		}
	}

	// Determine which field in the table to use
	if (dir == DATA_UP){
		tableField = "up";
	}
	else{
		tableField = "down";
	}

	// Calculate the average bandwidth for each hour range. Hour ranges that
	// have no data are discarded from the returned output.
	for (BWTimeRange& sr : allSpeedRanges){
		if (getAvgMetricFieldValue(BANDWIDTH_ISP_TABLE, tableField,
						sr.timeStamps, sr.avgSpeed) == 0){
			validSpeedRanges.push_back(sr);
		}
	}

	return validSpeedRanges;
}

/*
 * Orders a set of devices by their latency, where the first device
 * has the worst. A vector of tuples is returned where the first item
 * in each tuple is the average latency and the second is the device itself.
 */
vector<tuple<int, Device>> orderByLatency(vector<Device> devices,
		int secondsPeriod){
	vector<tuple<int, Device>> deviceLatency;

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Get the average latency for each device given
	for (Device device : devices){
		double avgLatency;
		vector<string> filters = {"device=\'"+device.getMacAddress()+"\'"};

		if(getAvgMetricFieldValue(LATENCY_DEVICE_TABLE, "avg_latency",
				timeFrom, timeNow, avgLatency, filters) != 0){
			// Ignore device if no average could be found
			continue;
		}

		deviceLatency.push_back(make_tuple(static_cast<int>(avgLatency), device));
	}

	// Sort devices by their latency in descending order
	sort(deviceLatency.begin(), deviceLatency.end(), compareDeviceMetricTuples);

	return deviceLatency;
}

/*
 * Orders a set of devices by their signal strength, where the first device
 * has the worst signal. A vector of tuples is returned where the first item
 * in each tuple is the average signal and the second is the device itself.
 */
vector<tuple<int, Device>> orderBySignalStrength(vector<Device> devices,
		int secondsPeriod){
	vector<tuple<int, Device>> deviceSignal;

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Get the average signal strength for each device given
	for (Device device : devices){
		if (device.isWireless() == false){
			continue;
		}

		double avgSignal;
		vector<string> filters = {"device=\'"+device.getMacAddress()+"\'"};

		if(getAvgMetricFieldValue(WIRELESS_SIGNAL_TABLE, "signal",
				timeFrom, timeNow, avgSignal, filters) != 0){
			// Ignore device if no average could be found
			continue;
		}

		deviceSignal.push_back(make_tuple(static_cast<int>(avgSignal), device));
	}

	// Sort devices by their signal strength in descending order
	sort(deviceSignal.begin(), deviceSignal.end(), compareDeviceMetricTuples);

	return deviceSignal;
}

/*
 * Orders a set of devices by their speed usage, where the first device is
 * uses the most speed. A vector of tuples is returned where the first item
 * in each tuple is the average speed and the second is the device itself.
 */
vector<tuple<int, Device>> orderBySpeedUsage(vector<Device> devices,
		DATA_DIRECTION dir, int secondsPeriod){
	string field;
	vector<tuple<int, Device>> deviceUsage;

	// Calculate the time range in Unix time
	time_t now = time(0);
	int timeNow = static_cast<int>(now);
	int timeFrom = timeNow - secondsPeriod;

	// Determine which field to use
	if (dir == DATA_UP){
		field = "bps_out";
	}
	else{
		field = "bps_in";
	}

	// Get the average speed for each device given
	for (Device device : devices){
		double avgSpeed;
		vector<string> filters = {"device=\'"+device.getMacAddress()+"\'"};

		if(getAvgMetricFieldValue(TRAFFIC_LOAD_DEVICES_TABLE, field,
				timeFrom, timeNow, avgSpeed, filters) != 0){
			// Ignore device if no average could be found
			continue;
		}

		deviceUsage.push_back(make_tuple(static_cast<int>(avgSpeed), device));
	}

	// Sort devices by their speed usage in descending order
	sort(deviceUsage.begin(), deviceUsage.end(), compareDeviceMetricTuples);

	return deviceUsage;
}


