/*
 * text.h
 *
 *  Created on: 27 Mar 2019
 *      Author: sgasper
 */

#ifndef TEXT_H_
#define TEXT_H_

#include <iostream>
#include <vector>
using namespace std;

const string NL = "<br/>";		// HTML new line character
const string TAB = "&emsp "; 	// HTML tab space tag

string convert24HrTo12Hr(int hour);
string generateOrderedString(vector<string> list, int numOfTabs=0,
		bool numbered=true, bool boldNumbers=true, bool boldContent=false,
		bool italicContent=false, bool lineGap=true);
string makeBold(string txt);
string makeItalic(string txt);

#endif /* TEXT_H_ */
