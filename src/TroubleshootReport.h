/*
 * TroubleshootReport.h
 *
 *  Created on: 8 Mar 2019
 *      Author: sgasper
 */

#ifndef TROUBLESHOOTREPORT_H_
#define TROUBLESHOOTREPORT_H_

#include <iostream>
#include <map>
#include <vector>
using namespace std;

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
using namespace rapidjson;

#include "debugnet.h"
#include "problems.h"

const string CONTEXT_LOG_HANDLE = "context";

// Holds details for every problem found
struct DetectedProblem {
	string problem;
	PROBLEM_SEVERITY severity;
	string advice;
};

// Holds information discovered during a troubleshooting session
class TroubleshootReport {
private:
	map<string, string> context;
	string queryName;
	Logger logger;
	vector<DetectedProblem> problems;
public:
	TroubleshootReport();
	TroubleshootReport(string queryName, Logger logger);
	void addContext(string contextName, bool contextValue);
	void addContext(string contextName, int contextValue);
	void addContext(string contextName, string contextValue);
	string getAsJsonStr();
	void reportProblem(string problem, PROBLEM_SEVERITY severity, string advice);
};



#endif /* TROUBLESHOOTREPORT_H_ */
