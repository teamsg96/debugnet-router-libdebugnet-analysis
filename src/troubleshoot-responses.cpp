/*
 * responses.cpp
 *
 *  Created on: 10 Mar 2019
 *      Author: sgasper
 */

#include "troubleshoot-responses.h"

/*
 * Get advice for bandwidth capacity problems
 */
string getBWCapacityProblemAdvice(vector<DATA_DIRECTION> dirsAtCap,
		vector<Device> onlineDevices, int secondsPeriod){
	double bwCap;
	double load;
	double capacityUsed;
	vector<string> advice;
	string currSpeedAdviceStr;

	// Determine which directions are at capacity
	bool downAtCap = find(dirsAtCap.begin(), dirsAtCap.end(), DATA_DOWN) != dirsAtCap.end();
	bool upAtCap = find(dirsAtCap.begin(), dirsAtCap.end(), DATA_UP) != dirsAtCap.end();

	currSpeedAdviceStr += "Contact your Internet service provider to increase your ";

	// Be specific about whether upload or download or both
	if (downAtCap == true && upAtCap == true){
		currSpeedAdviceStr += "upload and download ";
	}
	else if (upAtCap == true){
		currSpeedAdviceStr += "upload ";
	}
	else{
		currSpeedAdviceStr += "download ";
	}

	currSpeedAdviceStr += "speed. Currently you are getting ";

	// Add the current download speed for the period of time given.
	if (downAtCap == true){
		if (getIspBandwidthAvgStats(DATA_DOWN, bwCap, load, capacityUsed,
				secondsPeriod) == 0){
			currSpeedAdviceStr += to_string(static_cast<int>(bitsToMegaBits(bwCap))) + "Mbps download speeds";

		}
		else{
			// This will unlikely fail due to it not failing before to know its
			// at capacity.
			currSpeedAdviceStr += "an unknown download speed";
		}

	}

	if (downAtCap == true && upAtCap == true){
		currSpeedAdviceStr += " and ";
	}

	// Add the current upload speed for the period of time given.
	if (upAtCap == true){
		if (getIspBandwidthAvgStats(DATA_UP, bwCap, load, capacityUsed,
				secondsPeriod) == 0){
			currSpeedAdviceStr += to_string(static_cast<int>(bitsToMegaBits(bwCap))) + "Mbps upload speeds";
		}
		else{
			// This will unlikely fail due to it not failing before to know its
			// at capacity.
			currSpeedAdviceStr += "an unknown upload speed";
		}
	}

	currSpeedAdviceStr += ".";
	advice.push_back(currSpeedAdviceStr);

	// Give advice specific to when the capacity is reached for 'up' traffic.
	if (upAtCap == true){
		string upAtCapAdvice = string(
				"Try stopping or disabling applications on the networked devices "
				"that are likely to be using the most \'up speed\'. "
				"These will be applications that upload files/pictures/videos, "
				"host online gaming sessions, backup files (e.g. Onedrive, Dropbox) "
				"and provide video communication services "
				"(e.g. Skype, Facetime). Alternatively and if possible power off "
				"devices or disconnect them from the network. "
				"Below are the devices that are using the most \'up speed\', "
				"where the first device is the one that is producing the most. "
				"Attempt the advice given on each device, starting with the first:"
				);

		vector<tuple<int, Device>> devUsages = orderBySpeedUsage(onlineDevices,
				DATA_UP, secondsPeriod);

		// Display top devices that consume the most up speed. If the number
		// of devices is less than the 'max top' then stop at the size of the
		// number of devices.
		vector<string> upDevNames;
		for (unsigned int i = 0; i < MAX_TOP_DEVICES_IN_CAP_ADVICE && i < devUsages.size(); i++){
			upDevNames.push_back(get<1>(devUsages[i]).getHumanReadableName());
		}
		upAtCapAdvice += generateOrderedString(upDevNames, 1, true, false, true,
				true, false);

		advice.push_back(upAtCapAdvice);
	}

	// Give advice specific to when the capacity is reached for 'down' traffic.
	if (downAtCap == true){
		string downAtCapAdvice = string(
				"Try stopping or disabling applications on the networked devices "
				"that are likely to be using the \'down speed\'. "
				"These will be applications that download files/pictures/videos, "
				"do online gaming, stream videos (such as Netflix, Youtube "
				"and Amazon Prime) and provide video communication services "
				"(e.g. Skype, Facetime). Alternatively and if possible power off "
				"devices or disconnect them from the network. "
				"Below are the devices that are using the most \'down speed\', "
				"where the first device is the one that is consuming the most. "
				"Attempt the advice given on each device, starting with the first:"
				);

		vector<tuple<int, Device>> devUsages = orderBySpeedUsage(onlineDevices,
				DATA_DOWN, secondsPeriod);

		// Display top devices that consume the most up speed. If the number
		// of devices is less than the 'max top' then stop at the size of the
		// number of devices.
		vector<string> downDevNames;
		for (unsigned int i = 0; i < MAX_TOP_DEVICES_IN_CAP_ADVICE && i < devUsages.size(); i++){
			downDevNames.push_back(get<1>(devUsages[i]).getHumanReadableName());
		}

		downAtCapAdvice += generateOrderedString(downDevNames, 1, true, false, true,
				true, false);

		advice.push_back(downAtCapAdvice);
	}

	return generateOrderedString(advice);
}

/*
 * Get the description text for bandwidth capacity problems
 */
string getBWCapacityProblemDescription(PROBLEM_SEVERITY severity,
		vector<DATA_DIRECTION> dirsAtCap){
	string problemStr = "It was detected that the ";

	// Be specific about whether upload or download or both are at capacity
	if (dirsAtCap.size() == 2){
		problemStr += "upload and download ";
	}
	else if (dirsAtCap[0] == DATA_UP){
		problemStr += "upload ";
	}
	else{
		problemStr += "download ";
	}

	problemStr += "speed provided by your Internet service provider is ";

	// Give indication of severity of the problem
	if (severity == SEVERE_PROBLEM){
		problemStr += "not sufficient enough ";
	}
	else{
		problemStr += "\'almost\' not sufficient enough ";

	}

	problemStr += "to handle your networks usage.";

	return problemStr;
}

/*
 * Get advice for problems with device latency
 */
string getDeviceLatencyProblemAdvice(Device device){
	vector<string> advice;

	// Easiest thing to try, so suggest first
	advice.push_back("try restarting the device");

	// Give advice based on the connection method of the device
	if (device.getConnectionType() == CONN_OTHER){
		advice.push_back(
				"If the device is connected via Ethernet cable, try using"
				" a different cable, as the one you are using maybe damaged."
				);
		advice.push_back(
				"If the device is connected via Ethernet cable, try using "
				" a smaller cable than the one you are using."
				);
	}
	else if (device.getConnectionType() == CONN_WIFI_2_4){
		advice.push_back(
				"The device is currently connected to the routers 2.4Ghz WiFi, "
				"try connecting to the 5Ghz WiFi to achieve better speeds and "
				"a more stable connection. To do this go to your devices "
				"wireless network settings and search for available WiFi access "
				"points, select the access point that shares the same name as "
				"the router you are already connected to but with \'5G\' at "
				"the end of its name. Note, if the \'5G\' network doesn't "
				"appear for the device then it may not support connecting to"
				"5Ghz networks. Contact the manufacturer to find out more."
				);
	}

	if (device.isWireless() == true){
		advice.push_back(
				"The device is currently connected to the router through "
				"wireless. Try connecting the device to the router with an "
				"Ethernet cable."
		);
	}

	// Provide more generic 'last resort' advice
	advice.push_back("Restart the router");
	advice.push_back("Try disabling the anti-virus software and your device, "
					 "if slowness no longer persists then consider using "
					 "an alternative anti-virus package");

	return generateOrderedString(advice);
}

/*
 * Get a description of a device latency problem
 */
string getDeviceLatencyProblemDescription(){
	return string(
			"It seems that data is taking to long to travel from the router "
			"to the device."
	);
}

/*
 * Get advice for wireless signal problems
 */
string getDeviceWirelessSignalProblemAdvice(){
	vector<string> advice;

	advice.push_back(
			"Move the device closer to the router"
			);

	advice.push_back(
			"Ensure no objects are obstructing the router"
			);

	advice.push_back(
			"If its not possible to move the device closer then consider "
			"relocating the router closer to the device or purchase a WiFi "
			"booster or power line booster."
			);

	return generateOrderedString(advice);
}

/*
 * Get a description of the wireless signal problem for a device
 */
string getDeviceWirelessSignalProblemDescription(PROBLEM_SEVERITY severity){
	if (severity == SEVERE_PROBLEM){
		return string(
				"The wireless signal is very poor to the device."
		);
	}
	else{
		return string(
				"The wireless signal is poor to the device."
		);
	}
}

/*
 * Get advice for ISP latency issues
 */
string getIspLatencyProblemAdvice(PROBLEM_SEVERITY capacitySeverity){
	vector<string> advice;

	advice.push_back("Restart the router");

	// When problems occur with BW capacity then it is likely that it is the
	// reason for poor ISP latency.
	if (capacitySeverity == SEVERE_PROBLEM){
		advice.push_back("This problem often occurs when the Internet speed from " \
				"the Internet service provider is not enough to meet the " \
				"Internet needs of the household. Please ensure you have attempted " \
				"to try and following the prior advice given in relation to " \
				"insufficient upload/down speeds before proceeding any further.");
	}

	advice.push_back("Contact your Internet service provider to check for "\
			"problems on the line.");

	advice.push_back("Consider switching to fibre optic Internet if you are "\
			"on it already. This will significantly decrease the amount of time "\
			"it takes for data to travel from the Internet service provider to "\
			"your router");

	return generateOrderedString(advice);
}

/*
 * Get the description of ISP latency issues.
 */
string getIspLatencyProblemDescription(){
	return string(
			"Its taking longer than normal for your data to go from the router "
			"to the Internet service provider. "
			);
}

/*
 * Get advice for Net wide latency issues
 */
string getNetWideLatencyProblemAdvice(vector<Device> devicesWithLatProblems,
		PROBLEM_SEVERITY signalSeverity){
	vector<string> devicesOn24Ghz;
	vector<string> devicesOn5Ghz;
	vector<string> devicesOnOther;
	vector<string> advice;

	if (signalSeverity != SEVERE_PROBLEM){
		advice.push_back("This problem can often occur if multiple devices in "
				"the network are getting very poor wireless signal. Attempt "
				"following the advice for the problem previously mentioned "
				"and then attempt the remaining advice here");
	}

	// Easiest thing to try, so suggest first
	advice.push_back("Try restarting the router");

	for (Device device : devicesWithLatProblems){
		if (device.getConnectionType() == CONN_WIFI_5){
			devicesOn5Ghz.push_back(device.getHumanReadableName());
		}
		else if (device.getConnectionType() == CONN_WIFI_2_4){
			devicesOn24Ghz.push_back(device.getHumanReadableName());
		}
		else{
			devicesOnOther.push_back(device.getHumanReadableName());
		}
	}

	// Give advice based on the connection method of the device
	if (devicesOnOther.empty() == false){
		advice.push_back(
				"The following devices maybe using Ethernet cables, if they "
				"are try using different cables, as they could be damaged. "
				"Alternatively try using smaller cables. Devices:" +
				generateOrderedString(devicesOnOther, 1, false, false, true,
						true, false));
	}
	else if (devicesOn24Ghz.empty() == false){
		advice.push_back(
				"Multiple devices are currently connected to the routers "
				"2.4Ghz WiFi, try connecting them to the 5Ghz WiFi to achieve "
				"better speeds and a more stable connection. To do this go to "
				"a devices wireless network settings and search for available "
				"WiFi access points. Select the access point that shares the "
				"same name as the access point the device is already connected "
				"to but with \'5G\' at the end of its name. Note, if the \'5G\' "
				"network doesn't appear for the device, then it may not support "
				"connecting to 5Ghz networks. The following is a list of devices "
				"that are on the 2.4Ghz network that can be switched to the "
				"5Ghz network, try attempting the instructions on each device:" +
				generateOrderedString(devicesOn24Ghz, 1, false, false, true,
										true, false)
				);
	}

	if (devicesOn24Ghz.empty() == false || devicesOn5Ghz.empty() == false){
		advice.push_back(
				"The following devices are currently connected to the router "
				"through wireless. Try connecting them to the router using "
				"Ethernet cables, but only if they have Ethernet ports:"+
				generateOrderedString(devicesOn24Ghz, 1, false, false, true,
														true, false) +
				generateOrderedString(devicesOn5Ghz, 1, false, false, true,
														true, false)
		);
	}

	advice.push_back("Try disabling the anti-virus software on the devices, "
					 "if slowness no longer persists then consider using "
					 "an alternative anti-virus package.");

	return generateOrderedString(advice);
}

/*
 * Get the description for a latency problem occurring across the network
 */
string getNetWideLatencyProblemDescription(vector<Device> devicesWithLatProblems,
		int secondsPeriod){
	string problemStr = string(
			"It seems that data is taking to long to travel from the router "
			"to the majority of devices on the network. See below for "
			"affected devices, in the order of most affected to the least affected: "
	);

	// Display the devices that have poor latency in the order of device with
	// worst latency first.
	vector<string> devNames;
	for (tuple<int,Device> &devLatency : orderByLatency(
			devicesWithLatProblems, secondsPeriod) ){
		devNames.push_back(get<1>(devLatency).getHumanReadableName());
	}
	problemStr += generateOrderedString(devNames, 1, false, true, true,
			true, false);

	return problemStr;
}

/*
 * Get advice for wireless issues being experienced across the network
 */
string getNetWideWirelessSignalProblemAdvice(){
	vector<string> advice;

	advice.push_back(
			"Ensure no objects are obstructing the router"
			);

	advice.push_back(
			"Relocate your wireless devices closer to the router or "\
			"alternatively relocate the router to a position closer to the "\
			"other devices."
			);

	advice.push_back(
			"Consider purchasing a WiFi booster or power line booster."
			);

	return generateOrderedString(advice);
}

/*
 * Get the description text for a wireless signal problem occurring net-wide
 */
string getNetWideWirelessSignalProblemDescription(PROBLEM_SEVERITY severity,
		vector<Device> devicesWithSignalProblems, int secondsPeriod){
	string problemStr;

	if (severity == SEVERE_PROBLEM){
		problemStr = "The wireless signal is very poor across most of the network.";
	}
	else{
		problemStr = "The wireless signal is poor across most of the network.";
	}

	problemStr += " The following devices where found to have bad signal:";

	// Display the devices that have poor signal strength in the order of devices
	// with the worst signal strength.
	vector<string> devNames;
	for (tuple<int,Device> &devSignal : orderBySignalStrength(
			devicesWithSignalProblems, secondsPeriod) ){
		devNames.push_back(get<1>(devSignal).getHumanReadableName());
	}
	problemStr += generateOrderedString(devNames, 1, false, true, true,
			true, false);

	return problemStr;
}

