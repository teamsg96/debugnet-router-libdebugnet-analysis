/*
 * factual.h
 *
 *  Created on: 11 Mar 2019
 *      Author: sgasper
 */

#ifndef FACTUAL_H_
#define FACTUAL_H_

#include <cmath>
#include <iomanip>

#include <iostream>
using namespace std;

#include "analysis.h"
#include "debugnet.h"
#include "results.h"
#include "text.h"

struct FactualQueryResult{
	QUERY_RESULT_CODE code;
	string err;
	string factualStr;
};

// Current speed query
const int CURRENT_SPEED_SECONDS_PERIOD = 600;		// 10 Minutes

// ISP delivery query
const double SIMILAR_MARGIN_PERCENT = 10.0;			// Percent
const double CLOSE_TO_CAPACITY_MARK = 80.0;			// Percent
const int PERIOD_TO_CHECK_SERVICE_DELIVER = 604800;	// 1 week in seconds

// Time of day for best/worst speed query
const int DAYS_TO_CHECK_ISP_BW = 20;
const int NUM_OF_TIME_FRAMES_TO_SHOW = 3;
const int TIME_FRAME_SIZE = 2;	// Hours

FactualQueryResult queryGetCurrentInternetSpeed();
FactualQueryResult queryGetTimeOfDayInternetFastestSlowest();
FactualQueryResult queryGetIspPerformance(double expUpMbps, double expDownMbps);

#endif /* FACTUAL_H_ */
