/*
 * analysis.h
 *
 *  Created on: 13 Mar 2019
 *      Author: sgasper
 */

#ifndef ANALYSIS_H_
#define ANALYSIS_H_

#include <iostream>
using namespace std;

#include "debugnet.h"

const string QUERY_PROCESSING_LOG_FILE = "/var/log/debugnet-analysis.log";


// Represents upload and download traffic
enum DATA_DIRECTION {
	DATA_UP,
	DATA_DOWN
};

// Maps each direction enum to a string that represents it
const map<DATA_DIRECTION, string> DATA_DIR_TO_STRING = {
		{DATA_UP, "upload"},
		{DATA_DOWN, "download"}
};

struct BWTimeRange {
	int frmHour;
	int toHour;
	vector<tuple<int, int>> timeStamps;
	double avgSpeed;
};

bool compareDeviceMetricTuples(const tuple<int, Device>& left,
		const tuple<int, Device>& right);
bool compareSpeedTimeRangeStructForBest(const BWTimeRange& left,
		const BWTimeRange& right);
bool compareSpeedTimeRangeStructForWorst(const BWTimeRange& left,
		const BWTimeRange& right);
int getIspBandwidthAvgStats(DATA_DIRECTION dir, double& bwCap, double& load,
		double& capacityUsed, int secondsPeriod);
vector<BWTimeRange> getAvgIspBandwidthForTimesOfDay(DATA_DIRECTION dir,
		int daysToCheck, int hoursInRange);
vector<tuple<int, Device>> orderByLatency(vector<Device> devices,
		int secondsPeriod);
vector<tuple<int, Device>> orderBySignalStrength(vector<Device> devices,
		int secondsPeriod);
vector<tuple<int, Device>> orderBySpeedUsage(vector<Device> devices,
		DATA_DIRECTION dir, int secondsPeriod);



#endif /* ANALYSIS_H_ */
