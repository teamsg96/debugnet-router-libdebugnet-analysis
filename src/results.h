/*
 * results.h
 *
 *  Created on: 13 Mar 2019
 *      Author: sgasper
 */

#ifndef RESULTS_H_
#define RESULTS_H_

#include <iostream>
using namespace std;

// Codes that represent the outcome of a query process.
enum QUERY_RESULT_CODE{
	QUERY_SUCCESS,					// Completed successfully, no errors.
	QUERY_ERR,						// Some error occurred
	QUERY_ERR_DEVICE_NOT_FOUND,		// No device found with given MAC address.
};

#endif /* RESULTS_H_ */
