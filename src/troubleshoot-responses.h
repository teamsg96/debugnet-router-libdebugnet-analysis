/*
 * responses.h
 *
 *  Created on: 10 Mar 2019
 *      Author: sgasper
 */

#ifndef TROUBLESHOOT_RESPONSES_H_
#define TROUBLESHOOT_RESPONSES_H_

#include <iostream>
using namespace std;

#include "debugnet.h"
#include "problems.h"
#include "text.h"


const int MAX_TOP_DEVICES_IN_CAP_ADVICE = 3;

string getBWCapacityProblemAdvice(vector<DATA_DIRECTION> dirsAtCap,
		vector<Device> onlineDevices, int secondsPeriod);
string getBWCapacityProblemDescription(PROBLEM_SEVERITY severity,
		vector<DATA_DIRECTION> dirsAtCap);
string getDeviceLatencyProblemDescription();
string getDeviceLatencyProblemAdvice(Device device);
string getDeviceWirelessSignalProblemDescription(PROBLEM_SEVERITY severity);
string getDeviceWirelessSignalProblemAdvice();
string getIspLatencyProblemAdvice(PROBLEM_SEVERITY capacitySeverity);
string getIspLatencyProblemDescription();
string getNetWideLatencyProblemAdvice(vector<Device> devicesWithLatProblems,
		PROBLEM_SEVERITY signalSeverity);
string getNetWideLatencyProblemDescription(vector<Device> devicesWithLatProblems,
		int secondsPeriod);
string getNetWideWirelessSignalProblemAdvice();
string getNetWideWirelessSignalProblemDescription(PROBLEM_SEVERITY severity,
		vector<Device> devicesWithSignalProblems, int secondsPeriod);
#endif /* TROUBLESHOOT_RESPONSES_H_ */
