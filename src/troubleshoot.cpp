/*
 * troubleshoot.cpp
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */


#include "troubleshoot.h"

/*
 * Answers why a device might be regularly losing connection
 */
TSQueryResult queryWhyDoesConnectionDrop(Device device, int secondsOfinstability){
	TSQueryResult qr;
	PROBLEM_SEVERITY deviceLatencySeverity;
	PROBLEM_SEVERITY signalSeverity;

	Logger logger;
	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	// Initialise report and add the query parameters to the context.
	string tsTitleName = ("Why does the connection regularly drop to device \'" +
			device.getHumanReadableName() + "\'?");
	qr.report = TroubleshootReport(tsTitleName, logger);
	qr.report.addContext("deviceMac", device.getMacAddress());
	qr.report.addContext("instableTime", secondsOfinstability);
	qr.report.addContext("hostname", device.getHostname());
	qr.report.addContext("isWireless", device.isWireless());

	// The problem is likely to of persisted before the time given, so add a
	// small additional amount of time so that a greater sample of collected
	// metric data can be analysed.
	secondsOfinstability += ADDITIONAL_PERIOD;

	// Check wireless signal problems if the device is wireless
	if (device.isWireless() == true){
		signalSeverity = checkForDeviceWifiSignalProblems(device,
				secondsOfinstability, logger);
	}
	else{
		// If device is not wireless, then their is no problems with wireless
		// signal.
		signalSeverity = NO_PROBLEM;
	}

	// Check if their was a problem with wireless signal. If their was report
	// it. Otherwise check for problems with the devices latency. Bad wireless
	// signal is a likely cause for a connection to drop out, so their is no
	// point checking latency if the signal is bad.
	if (signalSeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getDeviceWirelessSignalProblemDescription(signalSeverity),
				signalSeverity,
				getDeviceWirelessSignalProblemAdvice());
	}
	else{
		deviceLatencySeverity = checkForDeviceLatencyProblems(device,
				secondsOfinstability, logger);

		// Report problems with device latency if their was a problem.
		if (deviceLatencySeverity != NO_PROBLEM){
			qr.report.reportProblem(
					getDeviceLatencyProblemDescription(),
					deviceLatencySeverity,
					getDeviceLatencyProblemAdvice(device));
		}
	}

	qr.code = QUERY_SUCCESS;
	return qr;
}

/*
 * Answers why a device would be experiencing slowness on the network.
 */
TSQueryResult queryWhyIsDeviceSlow(Device device, int secondsOfSlowness){
	TSQueryResult qr;
	PROBLEM_SEVERITY capacitySeverity;
	PROBLEM_SEVERITY deviceLatencySeverity;
	PROBLEM_SEVERITY ispLatencySeverity;
	PROBLEM_SEVERITY signalSeverity;

	Logger logger;
	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	// Initialise report and add the query parameters to the context.
	string tsTitleName = ("Why is the internet slow to device \'" +
			device.getHumanReadableName() + "\'?");
	qr.report = TroubleshootReport(tsTitleName, logger);
	qr.report.addContext("deviceMac", device.getMacAddress());
	qr.report.addContext("slowTime", secondsOfSlowness);
	qr.report.addContext("hostname", device.getHostname());
	qr.report.addContext("isWireless", device.isWireless());

	// The problem is likely to of persisted before the time given, so add a
	// small additional amount of time so that a greater sample of collected
	// metric data can be analysed.
	secondsOfSlowness += ADDITIONAL_PERIOD;

	// Check wireless signal problems if the device is wireless
	if (device.isWireless() == true){
		signalSeverity = checkForDeviceWifiSignalProblems(device, secondsOfSlowness,
				logger);
	}
	else{
		// If device is not wireless, then their is no problems with wireless
		// signal.
		signalSeverity = NO_PROBLEM;
	}

	// Check if their was a problem with wireless signal. If their was report
	// it. Otherwise check for problems with the devices latency. Bad wireless
	// signal as a big cause for poor device latency, so their is no point
	// checking it if the signal is bad.
	if (signalSeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getDeviceWirelessSignalProblemDescription(signalSeverity),
				signalSeverity,
				getDeviceWirelessSignalProblemAdvice());
	}
	else{
		deviceLatencySeverity = checkForDeviceLatencyProblems(device,
				secondsOfSlowness, logger);

		// Report problems with device latency if their was a problem.
		if (deviceLatencySeverity != NO_PROBLEM){
			qr.report.reportProblem(
					getDeviceLatencyProblemDescription(),
					deviceLatencySeverity,
					getDeviceLatencyProblemAdvice(device));
		}
	}

	// Check if the ISP bandwidth is capable of servicing the level of traffic
	// over 'slow time'. An artifact of this check is the directional in which
	// capacity problems where discovered.
	vector<DATA_DIRECTION> dirsAtCap;
	capacitySeverity = checkForBandwidthCapacityProblems(secondsOfSlowness,
			dirsAtCap, logger);
	if (capacitySeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getBWCapacityProblemDescription(capacitySeverity, dirsAtCap),
				capacitySeverity,
				getBWCapacityProblemAdvice(dirsAtCap, getAllOnlineDevices(),
						secondsOfSlowness));
	}

	ispLatencySeverity = checkForISPLatencyProblems(secondsOfSlowness, logger);

	// Report problems with ISP latency if their was a problem.
	if (ispLatencySeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getIspLatencyProblemDescription(),
				ispLatencySeverity,
				getIspLatencyProblemAdvice(capacitySeverity));

	}

	qr.code = QUERY_SUCCESS;
	return qr;
}

/*
 * Answers why the entire network would be experiencing slowness.
 */
TSQueryResult queryWhyIsEntireNetworkSlow(int secondsOfSlowness){
	Device device;
	TSQueryResult qr;
	PROBLEM_SEVERITY capacitySeverity;
	PROBLEM_SEVERITY latencySeverity;
	PROBLEM_SEVERITY ispLatencySeverity;
	PROBLEM_SEVERITY signalSeverity;

	Logger logger;
	logger.setFile(QUERY_PROCESSING_LOG_FILE);

	// Initialise report and add the query parameters to the context.
	qr.report = TroubleshootReport("Why is the internet on my network slow?", logger);
	qr.report.addContext("slowTime", secondsOfSlowness);

	// The problem is likely to of persisted before the time given, so add a
	// small additional amount of time so that a greater sample of collected
	// metric data can be analysed.
	secondsOfSlowness += ADDITIONAL_PERIOD;

	vector<Device> onlineDevices = getAllOnlineDevices();

	// Check for problems with wireless signal across the network.
	vector<Device> devicesWithSignalProblems;
	signalSeverity = checkForNetworkWifiSignalProblems(onlineDevices,
			secondsOfSlowness, devicesWithSignalProblems, logger);
	if (signalSeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getNetWideWirelessSignalProblemDescription(signalSeverity,
						devicesWithSignalProblems,
						secondsOfSlowness),
				signalSeverity,
				getNetWideWirelessSignalProblemAdvice());
	}

	// Check for problems with latency across the network.
	vector<Device> devicesWithLatProblems;
	latencySeverity = checkForNetworkLatencyProblems(onlineDevices,
			secondsOfSlowness, devicesWithLatProblems, logger);
	if (latencySeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getNetWideLatencyProblemDescription(devicesWithLatProblems,
						secondsOfSlowness),
				latencySeverity,
				getNetWideLatencyProblemAdvice(devicesWithLatProblems,
						signalSeverity));
	}

	// Check if the ISP bandwidth is capable of servicing the level of traffic
	// over 'slow time'. An artifact of this check is the directional in which
	// capacity problems where discovered.
	vector<DATA_DIRECTION> dirsAtCap;
	capacitySeverity = checkForBandwidthCapacityProblems(secondsOfSlowness,
			dirsAtCap, logger);
	if (capacitySeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getBWCapacityProblemDescription(capacitySeverity, dirsAtCap),
				capacitySeverity,
				getBWCapacityProblemAdvice(dirsAtCap, onlineDevices,
						secondsOfSlowness));
	}

	ispLatencySeverity = checkForISPLatencyProblems(secondsOfSlowness, logger);

	// Report problems with ISP latency if their was a problem.
	if (ispLatencySeverity != NO_PROBLEM){
		qr.report.reportProblem(
				getIspLatencyProblemDescription(),
				ispLatencySeverity,
				getIspLatencyProblemAdvice(capacitySeverity));
	}

	qr.code = QUERY_SUCCESS;
	return qr;
}
